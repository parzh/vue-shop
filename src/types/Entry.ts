export class Entry {
  /** Default entry price */
  public static readonly DEFAULT_PRICE = 0;

  // ***

  constructor(
    /** Entry name */
    public name: string,

    /** Entry unique identifier */
    public id: string,

    /** Entry price */
    public price: number = Entry.DEFAULT_PRICE
  ) { /* ... */ }
}
