import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

import App from './App';
import router from './router';
import store from './store';

import { quantityTexted, money, date } from './helpers';

// ***

Vue.use(Vuetify);
Vue.config.productionTip = false;

Vue.filter("texted", quantityTexted);
Vue.filter("money", money);
Vue.filter("date", date);

/* eslint-disable no-new */
global.app = new Vue({
  el: '#app',
  components: { App },
  template: "<App/>",
  router,
  store
});
