/** Stored cart access key */
export const STORED_CART_KEY = "vue-shop:cart";

/** Cart storage event timestamp access key */
export const TIMESTAMP_KEY = "vue-shop:cart:created";

// ***

/** Get cart stored in the LocalStorage */
export function getCart() {
  const cart = {};
  const stored = Object(JSON.parse(localStorage.getItem(STORED_CART_KEY)));

  if (TIMESTAMP_KEY in stored)
    Object.assign(cart, stored);

  return cart;
}

/**
 * Store cart in the LocalStorage
 * @arg {Map} cart The cart object
 */
export function saveCart(cart) {
  const serialized = { ...cart };

  serialized[TIMESTAMP_KEY] = Date.now();

  localStorage.setItem(STORED_CART_KEY, JSON.stringify(serialized));
}

/** Stored cart */
export const cart = getCart();
