import Vue from 'vue';
import Vuex from 'vuex';

import entriesRaw from "@/data/entries";
import { Entry } from "@/types/Entry";
import { quantityRanged } from "@/helpers";
import { cart, saveCart, TIMESTAMP_KEY } from "./storage";

// ***

const entries = {};
entriesRaw.map(raw => new Entry(...raw)).forEach(entry => entries[entry.id] = entry);

const store = {
  state: { entries, cart },

  getters: {
    /** Number of items in the cart */
    cartSize: state => Object.keys(state.cart).map(key => state.cart[key]).reduce((sum, { quantity }) => sum += quantity || 0, 0),

    /** Cart price */
    cartPrice: state =>
      Object.keys(state.cart)
        .filter(key => key !== TIMESTAMP_KEY)
        .map(key => [ state.entries[key], state.cart[key] ])
        .reduce((sum, [ entry, choice ]) => sum += (entry.price * choice.quantity) || 0, 0),

    /**
     * Check whether an item is in the cart
     * @arg {string} id Entry identifier
     */
    cartHasItem: state => id => id in state.cart
  },

  mutations: {
    /**
     * Change entry quantity in the cart
     * @arg {string} id Entry identifier
     * @arg {number} difference Difference in quantity
     */
    changeQuantity(state, { id, difference }) {
      const quantity = quantityRanged((id in state.cart? state.cart[id].quantity : 0) + difference);
      const changed = Date.now();

      if (!Number.isFinite(quantity) || quantity % 1)
        throw new Error("Quantity must be a natural number");

      else if (quantity)
        Vue.set(state.cart, id, { quantity, changed });

      else Vue.delete(state.cart, id);
    },

    /** Change entry quantity */
    clearCart(state) {
      for (const key in state.cart)
        if (state.cart.hasOwnProperty(key))
          Vue.delete(state.cart, key);
    }
  },

  actions: {
    /**
     * Add an item to the cart
     * @arg {string} id Entry identifier
     */
    async addItem(context, { id }) {
      await context.commit("changeQuantity", { id, difference: +1 });
      saveCart(context.state.cart);
    },

    /**
     * Remove item from the cart
     * @arg {string} id Entry identifier
     */
    async removeItem(context, { id }) {
      await context.commit("changeQuantity", { id, difference: -1 });
      saveCart(context.state.cart);
    },

    /** Clear cart */
    async clearCart(context) {
      await context.commit("clearCart");
      saveCart(context.state.cart);
    },

    /** Purchase */
    async purchase(context) {
      context.dispatch("clearCart"); // TODO: replace by an actual purchase logic
    }
  }
};

Vue.use(Vuex);

export default new Vuex.Store({ ...store, strict: process.env.NODE_ENV === 'development' });
