import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/pages/home.page';
import CartPage from '@/pages/cart.page';

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      name: 'HomePage',
      path: '/',
      component: HomePage,
      meta: {
        title: "Home Page"
      }
    },
    { path: '/home', redirect: { name: 'HomePage' } },
    { path: '/main', redirect: { name: 'HomePage' } },
    { path: '/root', redirect: { name: 'HomePage' } },

    {
      name: 'CartPage',
      path: '/cart',
      component: CartPage,
      meta: {
        title: "Cart Page"
      }
    },

    // ***

    { path: '*', redirect: { name: 'HomePage' } } // fallback
  ]
});

export default router;
