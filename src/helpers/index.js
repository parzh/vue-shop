/** Bring any number to [0; Infinity] range */
export const quantityRanged = number => Math.min(Infinity, Math.max(0, number));

/** Textual representation of a quantity */
export const quantityTexted = number => number + (number === 1? " item" : " items");

/** Show number as money amount */
export const money = amount => new Intl.NumberFormat("en", { style: "currency", currency: "USD" }).format(amount);

/** Show timestamp as localized date */
export const date = timestamp => new Date(timestamp).toLocaleDateString("en", { month: "short", day: "numeric", hour: "numeric", minute: "2-digit", second: "2-digit" });
